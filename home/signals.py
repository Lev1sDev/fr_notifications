from apscheduler.schedulers.background import BackgroundScheduler
from django.db.models.signals import post_save
from django.dispatch import receiver

from .models import Distribution
from .utils import send_message


@receiver(post_save, sender=Distribution)
def mailing_request(sender, instance, created, **kwargs):
    if created:
        scheduler = BackgroundScheduler()
        scheduler.start()
        if instance.to_send:
            scheduler.add_job(
                send_message,
                args=[instance.pk],
                id=f"job",
                max_instances=1,
                replace_existing=True,
            )
        else:
            scheduler.add_job(
                send_message,
                next_run_time=instance.date_start,
                args=[instance.pk],
                id=f"future_job",
                max_instances=1,
                replace_existing=True,
            )
