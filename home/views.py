from django.shortcuts import render
from rest_framework import viewsets, mixins
from .models import Distribution, Client, Message
from .serializers import DistributionSerializer, ClientSerializer


def home(request):
    data = {}
    return render(request, 'home.html', data)


class DistributionAPI(viewsets.ModelViewSet):
    queryset = Distribution.objects.all()
    serializer_class = DistributionSerializer


class ClientAPI(viewsets.GenericViewSet,
                mixins.CreateModelMixin,
                mixins.UpdateModelMixin,
                mixins.DestroyModelMixin):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer
