from django.urls import path, include
from rest_framework import routers

from .views import home, DistributionAPI, ClientAPI

router = routers.DefaultRouter()
router.register(r'distribution', DistributionAPI, basename='distribution')
router.register(r'client', ClientAPI, basename='client')

urlpatterns = [
    path('', home, name='home'),
    path('api/', include(router.urls)),
]
