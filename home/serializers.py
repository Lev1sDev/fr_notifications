from rest_framework import serializers
from .models import Distribution, Client, Message


class DistributionSerializer(serializers.ModelSerializer):
    messages_statistics = serializers.SerializerMethodField()

    class Meta:
        model = Distribution
        fields = '__all__'

    def get_messages_statistics(self, obj):
        sent_count = obj.messages.filter(status=Message.SUCCESS).count()
        error_count = obj.messages.filter(status=Message.ERROR).count()
        unsent_count = obj.messages.filter(status=Message.UNSENT).count()
        return [
            {'name': 'Количество успешно отправленных сообщений', 'result': sent_count},
            {'name': 'Количество сообщений c ошибкой отправки', 'result': error_count},
            {'name': 'Количество неотправленных сообщений', 'result': unsent_count},
        ]


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = '__all__'
