from datetime import timedelta

from django.test import TestCase
from django.utils import timezone

from home.models import Client, Distribution


class TestAPI(TestCase):
    API_CLIENT = '/api/client/'
    API_DISTRIBUTION = '/api/distribution/'

    def test_api_client(self):
        count = 0
        data = {
            'phone_number': '89990001122',
            'code_operator': '899',
            'tag': 'Tag',
            'time_zone': 'UTC',
        }
        # проверка валидации номера телефона
        response = self.client.post(
            self.API_CLIENT, data, content_type='application/json'
        )
        error_message = 'Номер телефона должен быть в формате 7XXXXXXXXXX'
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json()['phone_number'][0], error_message)

        # проверка создания клиента
        data['phone_number'] = '79990001122'
        response = self.client.post(
            self.API_CLIENT, data, content_type='application/json'
        )
        self.assertEqual(response.status_code, 201)
        self.assertEqual(Client.objects.count(), count + 1)

        # проверка обновления клиента
        client_id = response.json()['id']
        data = {
            'tag': 'New Tag',
        }
        response = self.client.patch(
            f'{self.API_CLIENT}{client_id}/',
            data,
            content_type='application/json'
        )
        self.assertEqual(response.json()['tag'], 'New Tag')
        self.assertEqual(response.status_code, 200)

        # проверка удаления клиента
        response = self.client.delete(
            f'{self.API_CLIENT}{client_id}/',
            data,
            content_type='application/json'
        )
        self.assertEqual(response.status_code, 204)
        self.assertEqual(Client.objects.count(), count)

    def test_api_distribution(self):
        dt_stop = timezone.now() + timedelta(hours=5)
        data = {
            'dt_start': timezone.now(),
            'dt_stop': dt_stop,
            'message_text': 'Message',
            'client_code': '999',
            'client_tag': '111',
        }
        count = 0
        # создание объекта рассылки
        response = self.client.post(
            self.API_DISTRIBUTION, data, content_type='application/json'
        )
        self.assertEqual(response.status_code, 201)
        self.assertEqual(Distribution.objects.count(), count + 1)

        # проверка обновления рассылки
        distribution_id = response.json()['id']
        data = {
            'message_text': 'New Message',
        }
        response = self.client.patch(
            f'{self.API_DISTRIBUTION}{distribution_id}/',
            data,
            content_type='application/json'
        )
        self.assertEqual(response.json()['message_text'], 'New Message')
        self.assertEqual(response.status_code, 200)

        # проверка удаления рассылки
        response = self.client.delete(
            f'{self.API_DISTRIBUTION}{distribution_id}/',
            data,
            content_type='application/json'
        )
        self.assertEqual(response.status_code, 204)

