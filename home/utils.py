import requests

from urllib.parse import urljoin
from django.db.models import Q
from django.utils import timezone

from .models import Distribution, Client, Message
from django.conf import settings

URL = settings.PROBE_URL
TOKEN = settings.PROBE_TOKEN


def send_message(distribution_pk):

    header = {
        'Authorization': f'Bearer {TOKEN}',
        'Content-Type': 'application/json'}

    distribution = Distribution.objects.filter(pk=distribution_pk).first()

    try:
        clients = Client.objects.filter(
            Q(code_operator=distribution.client_code)
            | Q(tag=distribution.client_tag)
        )
    except AttributeError:
        return 'Distribution object is None'

    for client in clients:
        dt = timezone.localtime(timezone.now())
        if dt > distribution.dt_stop:
            return 'Time out'
        message = Message.objects.create(
            distribution=distribution,
            client=client,
            status=Message.UNSENT
        )
        data = {
            "id": message.id,
            "phone": client.phone_number,
            "text": distribution.message_text
        }

        try:
            url = urljoin(URL, str(message.id))
            response = requests.post(url=url, headers=header, json=data, timeout=10)

        except Exception as e:
            message.status = Message.ERROR
            message.save()
            return e

        if response.status_code == 200:
            message.status = Message.SUCCESS
            message.save()
