from django.core.validators import RegexValidator
from django.db import models
from django.utils import timezone
from django.utils.timezone import now


class Distribution(models.Model):
    dt_start = models.DateTimeField(
        'Дата и время запуска рассылки', default=now
    )
    dt_stop = models.DateTimeField('Дата и время окончания рассылки')
    client_code = models.CharField('Код мобильного оператора', max_length=3)
    client_tag = models.CharField('Тэг', max_length=100)
    message_text = models.TextField('Сообщение клиенту')

    @property
    def to_send(self):
        dt = timezone.localtime(timezone.now())
        if self.dt_start <= dt <= self.dt_stop:
            return True
        return False

    class Meta:
        verbose_name = 'Рассылка'
        verbose_name_plural = 'Рассылки'


class Client(models.Model):
    phone_number = models.CharField(
        'Номер телефона', max_length=11,
        validators=[
            RegexValidator(
                regex=r'^7\d{10,11}$',
                message='Номер телефона должен быть в формате 7XXXXXXXXXX'
            ),
        ]
    )
    code_operator = models.CharField('Код мобильного оператора', max_length=3)
    tag = models.CharField('Тэг', max_length=100)
    time_zone = models.CharField('Часовой пояс', max_length=100)

    class Meta:
        verbose_name = 'Клиент'
        verbose_name_plural = 'Клиенты'


class Message(models.Model):

    UNSENT = 'Unsent'
    SUCCESS = 'OK'
    ERROR = 'Error'

    STATUSES = (
        (UNSENT, 'Unsent'),
        (SUCCESS, 'OK'),
        (ERROR, 'Error'),
    )

    dt_created = models.DateTimeField(
        'Дата и время создания (отправки)', auto_now_add=True, blank=False
    )
    status = models.CharField(
        'Статус отправки', max_length=7, choices=STATUSES
    )
    distribution = models.ForeignKey(
        Distribution, verbose_name='Рассылка', on_delete=models.CASCADE,
        related_name='messages'
    )
    client = models.ForeignKey(
        Client, verbose_name='Клиент', on_delete=models.CASCADE, related_name='messages'
    )

    class Meta:
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'
